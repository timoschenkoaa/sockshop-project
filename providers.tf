provider "google" {
  credentials = file("./creds/serviceaccount.json")
  project     = "k8s-project"
  region      = "us-central1"
}

provider "google-beta" {
  credentials = file("./creds/serviceaccount.json")
  project     = "k8s-project"
  region      = "us-central1"
}

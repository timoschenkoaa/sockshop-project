module "gke" {
  source                            = "git::https://github.com/terraform-google-modules/terraform-google-kubernetes-engine.git"
  project_id                        = "trim-bonsai-326906"
  name                              = "k8s-cluster"
  region                            = "us-central1"
  zones                             = ["us-central1-c"]
  network                           = "default"
  subnetwork                        = "default"
  ip_range_pods                     = ""
  ip_range_services                 = ""
  create_service_account            = false
  service_account                   = "terraform@trim-bonsai-326906.iam.gserviceaccount.com"
  http_load_balancing               = false
  horizontal_pod_autoscaling        = true
  disable_legacy_metadata_endpoints = false
  remove_default_node_pool          = true
  logging_service                   = "none"
  monitoring_service                = "none"

  node_pools = [
     {
      name                          = "managers"
      machine_type                  = "n1-standard-2"
      node_locations                = "us-central1-c"
      min_count                     = 2
      max_count                     = 3
      local_ssd_count               = 0
      disk_size_gb                  = 35
      disk_type                     = "pd-standard"
      image_type                    = "COS"
      auto_repair                   = true
      auto_upgrade                  = true
      service_account               = "terraform@trim-bonsai-326906.iam.gserviceaccount.com"
      preemptible                   = false
      initial_node_count            = 2
      enable_integrity_monitoring   = false
         
    },
    {
      name                          = "workers"
      machine_type                  = "n1-standard-2"
      node_locations                = "us-central1-c"
      min_count                     = 2
      max_count                     = 3
      local_ssd_count               = 0
      disk_size_gb                  = 35
      disk_type                     = "pd-standard"
      image_type                    = "COS"
      auto_repair                   = true
      auto_upgrade                  = true
      service_account               = "terraform@trim-bonsai-326906.iam.gserviceaccount.com"
      preemptible                   = false
      initial_node_count            = 2
      enable_integrity_monitoring   = false
        
    },
  ]
  
  node_pools_oauth_scopes = {
    all = []
   
  }

  node_pools_labels = {
    
    managers = {
      manager = true
    }

    workers = {
      worker = true
    }
  }
}

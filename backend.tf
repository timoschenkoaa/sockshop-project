terraform { 
  backend "gcs" { 
    bucket      = "trim-bonsai-326906-tfstate" 
    credentials = "./creds/serviceaccount.json" 
  } 
}